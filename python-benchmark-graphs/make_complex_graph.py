
#!/usr/bin/env python3
import os
import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
from statistics import *

TYPE_LAYER = ('0','1', '5', '10','volumen')
TYPE_TEST=('write', 'read')
TYPE_DDTEST=('throughput', 'latency')
SIZE=('1','8', '32', '128')
STORAGE_DRIVER=None
STORAGE_DRIVERS = ('aufs', 'overlay', 'overlay2')

def average_from_file(path):
    try:
        with open(path) as f:
            lines = [float(line) for line in f.readlines()]
        #return min(lines)
        return '{:0.2f}'.format(mean(lines))
    except FileNotFoundError:
        return 0
def seconds_average_from_file(path):
    try:
        with open(path) as f:
            lines = [float(line) for line in f.readlines()]
        #return min(lines)
        return '{:0.3f}'.format(mean(lines)/1000)
    except FileNotFoundError:
        return 0    
def throughput_from_file(path):
    try:
        with open(path) as f:
            results = []
            b_s=None
            for line in f.readlines():
                arr=line.split(',')
                if len(arr)==2:
                    b_s=((float(arr[0])/float(arr[1]))/1024)/1024
                    results.append(b_s)
                elif len(arr)==3:
                    b_s=((float(arr[0])/float(arr[1]+"."+arr[2]))/1024)/1024
                    results.append(b_s)
        if b_s != None:
            #print (results)       
            return '{:0.2f}'.format(mean(results))
        else:
            return 0
    except FileNotFoundError:
        return 0
def latency_from_file(path,count):
    try:
        with open(path) as f:
            results = []
            ms_latency=None
            for line in f.readlines():
                arr=line.split(',')
                if len(arr)==2:
                    ms_latency=(float(arr[1])/float(count))*1000
                    results.append(ms_latency)                  
                elif len(arr)==3: #Time number 0,000
                    ms_latency=(float(arr[1]+"."+arr[2])/float(count))*1000                   
                    results.append(ms_latency)
        if ms_latency != None:   
            return '{:0.2f}'.format(mean(results))
        else:
            return 0
    except FileNotFoundError:
        return 0

def add_value_labels(ax, spacing=5):
    """Add labels to the end of each bar in a bar chart.

    Arguments:
        ax (matplotlib.axes.Axes): The matplotlib object containing the axes
            of the plot to annotate.
        spacing (int): The distance between the labels and the bars.
    """

    # For each bar: Place a label
    for rect in ax.patches:
        # Get X and Y placement of label from rect.
        y_value = rect.get_height()
        x_value = rect.get_x() + rect.get_width() / 2

        # Number of points between bar and label. Change to your liking.
        space = spacing
        # Vertical alignment for positive values
        va = 'bottom'

        # If value of bar is negative: Place label below bar
        if y_value < 0:
            # Invert space to place label below
            space *= -1
            # Vertically align label at top
            va = 'top'

        # Use Y value as label and format number with one decimal place
        label = "{:.2f}".format(y_value)

        # Create annotation
        ax.annotate(
            label,                      # Use `label` as label
            (x_value, y_value),         # Place label at end of the bar
            xytext=(0, space),          # Vertically shift label by `space`
            textcoords="offset points", # Interpret `xytext` as offset in points
            ha='center',                # Horizontally center label
            va=va)                      # Vertically align label differently for
                                        # positive and negative values.

#
# Test 2 Overall
#
def make_graph_overall_test2(type_test):
    width = 1
    label_format = '{:,.0f}'
    fig = Figure(figsize=(10, 5))
    FigureCanvasAgg(fig)
    ax = fig.add_subplot(1, 1, 1)
    x = np.array([float(i)*(len(TYPE_LAYER)+1) for i in range(len(STORAGE_DRIVERS))])
    bars = []

    for type_layer, color in zip(TYPE_LAYER, 'mbcgr'):
        means = []
        for driver in STORAGE_DRIVERS:
            means.append(seconds_average_from_file(os.path.join(
                '../my-results/image-1-{}/test2/'.format(driver),
                '{}-{}-{}'.format(type_test, 'small-file', type_layer),
            )))
        #Necessary float data for to get  y  ordered labels    
        bars.append(ax.bar(x, [float(s) for s in means], 0.95, color=color))
        x += width

    ax.set_title('Times of Test({}) - {}'.format(type_test,"10000 Small files (512 bytes)"))
    ax.set_ylabel('Average(s) for completion')
    ax.set_xticklabels(STORAGE_DRIVERS,rotation="horizontal", size=10)
    ax.set_xticks(x - 2.5)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,box.width, box.height * 0.9])
    ax.set_xlabel('Number of Layer')
    ax.legend((bar[0] for bar in bars), TYPE_LAYER, loc='upper center',
            bbox_to_anchor=(0.5, -0.11),
            fancybox=True, shadow=True, ncol=5)

    fig.savefig(os.path.join('graphs', 'mygraph-{}-{}'.format(type_test,"overall-small-files") + '.png'))   

def make_graph_overall_ddtest(type_test):
    width = 1
    label_format = '{:^10}'
    fig = Figure(figsize=(12, 8))
    FigureCanvasAgg(fig)
    ax = fig.add_subplot(1, 1, 1)
    TYPE_DDLAYER=('0','1', '5', '10',"volumen")
    x = np.array([float(i)*(len(TYPE_DDLAYER)+1) for i in range(len(STORAGE_DRIVERS))])
    bars = []
    label_y="";
    if type_test =='throughput':
        label_y="Average(MB/s) for completion"
    elif type_test =='latency':
        label_y="Average(ms) Latency"
    
    for type_layer, color in zip(TYPE_DDLAYER, 'mbcgr'):
        avarages=[]
        for driver in STORAGE_DRIVERS:
            folder_results='../my-results/image-1-{}/dd-test-results/'.format(driver)
            if type_test =='throughput':
                avarages.append(throughput_from_file(os.path.join(
                    folder_results,
                    '{}-{}'.format(type_test,type_layer),
                )))
            elif type_test =='latency':
                 avarages.append(latency_from_file(os.path.join(
                    folder_results,
                    '{}-{}'.format(type_test,type_layer),
                ),1000))
        #Necessary float data for to get  y  ordered labels           
        bars.append(ax.bar(x,[float(avarage) for avarage in avarages], 0.9, color=color))
        x += width
    
    ax.set_title('Times of Test({}) - {}'.format(type_test,"UFS Storage Divers"))
    ax.set_ylabel(label_y)
    ax.set_xticklabels(STORAGE_DRIVERS,rotation="horizontal", size=12)
    ax.set_xticks(x - 1.2)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,box.width, box.height * 0.9])
    ax.set_xlabel('Number of Layer ')
    ax.legend((bar[0] for bar in bars), TYPE_DDLAYER, loc='upper center',
            bbox_to_anchor=(0.5, -0.05),
            fancybox=True, shadow=True, ncol=5)
    add_value_labels(ax)

    fig.savefig(os.path.join('graphs', 'mygraph-{}-overall'.format(type_test) + '.png'))

def main(argv=None):
    #Overall Test2
    for type in TYPE_TEST:
        make_graph_overall_test2(type)
    for type in TYPE_DDTEST:
        make_graph_overall_ddtest(type)
    print("[CREATED] GRAPH READ AND WRITES SMALL FILES BY STORAGE DRIVERS AND LAYER")
    print("[CREATED] GRAPHS THROUGHPUT AND LATENCY BY LAYER")
   

if __name__ == '__main__':
    sys.exit(main())