#!/bin/bash
# Docker file based on base
dockerfileTest1=Dockerfile.test
imgTagBase=manu/mybaseimage:1.0
imgTagTest1=manu/test1:1.0
#Volumen with same data that containers big-files and small-files
nameVolumen=data_test
#Containers name
name1=Test1

#
# Results in ./test1/ folder
#
folder_results="test1/"
name_volumen_results="myresults"



#
# === MAIN ===
#
repeats=$1
if [ $(id -u) -ne 0 ]; then
	echo "[WARN] Run this script as a Root user only" >&2
	exit 1
fi
re='^[0-9]+$'
if ! [[ $repeats =~ $re ]] ; then
   echo "[ERROR] Not a number (Set default: 1 repeat)" >&2;
   repeats=1
fi

# --------------------------------------------------------------------------
# Check:
#        -1 Volumen $nameVolumen exists
#        -2 Image with $imgTagTest1 exist
#        -3 $name1 container is STOPED
# --------------------------------------------------------------------------
check(){
    if [ ! -d "/var/lib/docker/volumes/$name_volumen_results/_data/"  ] || [[ "$(docker images -q $imgTagTest1 2> /dev/null)" == "" ]] ; then  
        echo -e "[ERROR] Image for test not found\n [SOLVE] Try to launch docker.build.sh";
    fi
    matchingStarted=$(docker ps --filter="name=$name1" -q | xargs)
	if [[ -n $matchingStarted ]]; then
		docker stop $matchingStarted		
		echo "[STOP] $name1 container running then stop"
	fi    
}
# --------------------------------------------------------------------------
# Folder Results:
#        -$(pwd)/image-1-$storage_driver/folder_results
# --------------------------------------------------------------------------
storage_driver=$(docker info 2>> /dev/null| grep 'Storage Driver:' | awk -F ':' '{print $2}'| tr -d '[:blank:]')
parent_folder="$(pwd)/image-1-$storage_driver"

if [ -d "$parent_folder"  ]; then rm -f $parent_folder/* 2>>/dev/null;fi
mkdir -p $parent_folder/$folder_results; chmod 777 -R $parent_folder;chmod 777 -R $parent_folder/*;

# --------------------------------------------------------------------------
# TEST1:
#        -Default command:  ./benchmark-command.sh -t 1 -s all
#             -t Type of test 1
#             -s set all path routes or layers
# --------------------------------------------------------------------------
bench_command=" ./benchmark-command.sh -t 1 -s all"
check
for (( i = 1; i <= $repeats ; i++ ));do
    docker run --rm \
        --privileged \
        -v $nameVolumen:/usr/src/app/myvolumen \
        -v /var/lib/docker/volumes/myresults/_data/image-2:/usr/src/app/results \
        --name=$name1 $imgTagTest1 bash -c "$bench_command"
done

#Copy to current folder
cp -r /var/lib/docker/volumes/myresults/_data/image-2/* $parent_folder/$folder_results
#Delete current results
rm -r /var/lib/docker/volumes/myresults/_data/image-2/*
#Change permissions
chmod 666 -R $parent_folder/$folder_results*
