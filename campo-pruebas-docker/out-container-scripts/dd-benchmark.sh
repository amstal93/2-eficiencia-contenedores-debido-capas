#!/bin/bash
# Docker file based on base
dockerfileTest1=Dockerfile.test
imgTagBase=manu/mybaseimage:1.0
imgTagTest1=manu/test1:1.0
#Volumen with same data that containers big-files and small-files
nameVolumen=data_test
#Containers name
name1=Test1

# --------------------------------------------------------------------------
# DD Info
#    -The dsync option will flush the data after every block
#    -The direct option will have similar results as dsync, only without polluting the filesystem cache with the data.
#       (saves some cpu overhead)
#   - It's a good option to test HDD/SSD perfomance(The oflag=direct option is used to disable disk caching).
#        *For our purpose it makes no sense*
# --------------------------------------------------------------------------
#
# Results in ./dd-test-results folder
#
level_layer=([10]='/test' [5]='/test2' [1]='/test3' [0]='/usr/src/app/test-current-layer')

#Filter to get only bytes and seconds and remove horizontal spaces
dd_filter=" | tail -1 | awk -F ' s' '{print \$1}'| awk -v FS='(bytes|copied)' '{print \$1 \$3}' | tr -d '[:blank:]'"
checktest() {
	results=$1
	if [ $results != 0 ];then
		echo "[ERROR] Error occured while launch some test: $*"
		exit 127
	fi
}
#
# === MAIN ===
#
repeats=$1
if [ $(id -u) -ne 0 ]; then
	echo "[WARN] Run this script as a Root user only" >&2
	exit 1
fi
re='^[0-9]+$'
if ! [[ $repeats =~ $re ]] ; then
   echo "error: Not a number" >&2;
   echo "Default: 1 repeat" >&2;
   repeats=1
fi
#
# Get current storage driver
#
storage_driver=$(docker info 2>> /dev/null| grep 'Storage Driver:' | awk -F ':' '{print $2}'| tr -d '[:blank:]')
parent_folder="$(pwd)/image-1-$storage_driver"
#
# Create result folders and set permissions
#
if [ -d "$parent_folder"  ]; then rm -f $parent_folder/* 2>>/dev/null;fi
mkdir -p $parent_folder/dd-test-results/; chmod 777 -R $parent_folder;chmod 777 -R $parent_folder/*;


for t in "throughput" "latency";do
    #Change size and count
    if [[ $t == *"latency"* ]]; then params=" bs=512 count=1000 ";else params=" bs=1G count=1 ";fi
    echo -e "\n[1] Test[$t] host..."
    #Test on host
    for (( i = 1; i <= $repeats ; i++ ));do
       bash -c "dd if=/dev/zero of=/var/lib/docker/volumes/$nameVolumen/_data/test_container1.img $params oflag=dsync 2>&1 $dd_filter >> $parent_folder/dd-test-results/$t-volumen"
       checktest $?
    done
    #Test in diferents location
    for layer in "${!level_layer[@]}"; do
    echo -e "\n[1] Test[${level_layer[layer]}] $name1 (container) ..."
        dd_command=" dd if=/dev/zero of=${level_layer[layer]}/test_container1.img $params oflag=dsync  2>&1"
        init_current_layer=""
        if (( $layer == 0 ));then init_current_layer="mkdir -p /usr/src/app/test-current-layer &&";fi
    
        for (( i = 1; i <= $repeats ; i++ ));do     
            docker run --rm \
                --privileged \
                -v $nameVolumen:/usr/src/app/myvolumen \
                --name=$name1 $imgTagTest1 bash -c "$init_current_layer $dd_command $dd_filter" >> $parent_folder/dd-test-results/$t-$layer
        done
    done
done

#Change permissions
chmod 777 -R $parent_folder/*
chmod 777 -R $parent_folder/$folder_results*