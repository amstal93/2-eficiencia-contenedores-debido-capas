#!/bin/bash
# 
# Example launch: sudo ./benchmark-command.sh -n 3 -c 'dd if=/dev/zero of=output.dat  bs=24M  count=1'
#                 sudo ./benchmark-command.sh -n 5 -o test_tree.csv -c 'tree /home' -t 1
# [Note] It's necessary SUDO
#

repeats=10
output_file='benchmark_resultss.csv'
output_ddfile='ddbenchmark_resultss.csv'
command_to_run=''
simple=${environment:-false}
checktest() {
	results=$1
	if [ $results != 0 ];then
		echo "[ERROR] Error occured while launch some test: $*"
		exit 127
	fi
}
checkpacket() {
	results=$1
	if [ $results != 0 ];then
		echo "[ERROR] Errors occured while install packet: $2"
		exit 127
	fi
}
check(){
	if [ $1 != 0 ];then
		echo "[ERROR] Errors occured: $2"
		exit 127
	fi
}
#
# Run command like Tree: time tree <folder>
# Available to run another command
#
run_tests() {
    # --------------------------------------------------------------------------
    # Install Requeriments
    # --------------------------------------------------------------------------
    command -v tree >/dev/null 2>&1 || { echo >&2 "Benchmark requires tree but it's not installed. Installing..."; sudo apt-get install tree >/dev/null 2>&1; checkpacket $? tree; }
    # --------------------------------------------------------------------------
    # Benchmark loop
    # --------------------------------------------------------------------------
    echo '[LAUNCH] Benchmarking ' $command_to_run '...';
    echo '======' $command_to_run '======' >> $output_file;

    # Run the given command [repeats] times
    for (( i = 1; i <= $repeats ; i++ )); do
        #Indicator
        p=$(( $i * 100 / $repeats))
        l=$(seq -s "+" $i | sed 's/[0-9]//g')

        # runs time function for the called script, output in a comma seperated
        # format output file specified with -o command and -a specifies append
        # Note:
        # Avoid output > /dev/null 2>&1 ccept and discard all input without output
        # --format='%C took %e seconds'
        /usr/bin/time -f "%E,%U,%S" -o ${output_file} -a ${command_to_run} > /dev/null 2>&1
        #
        # E: Elapsed real (wall clock) time used by the process, in [hours:]minutes:seconds
        # U: Total number of CPU-seconds that the process used directly (in user mode), in seconds.
        # K: Total number of CPU-seconds used by the system on behalf of the process (in kernel mode), in seconds.
        
        #Clear RAM Memory Cache, Buffer and Swap Space on Linux
        sync && echo 3 > /proc/sys/vm/drop_caches
        #Indicator
        echo -ne ${l}' ('${p}'%) \r'
    done;
}

# --------------------------------------------------------------------------
# Test specific pattern of dd command
# --------------------------------------------------------------------------
run_ddtests() {
    #dev/zero is used to create dummy files or swap
    #Small IO
    #    dd if=/dev/zero of=/test/testfile.img bs=512 count=1000 oflag=direct
    #Large IO
    #    dd if=/dev/zero of=/test/testfile.img bs=1G count=1 oflag=direct

    # --------------------------------------------------------------------------
    # Install Requeriments
    # --------------------------------------------------------------------------
    command -v dd >/dev/null 2>&1 || { echo >&2 "Benchmark requires dd but it's not installed. Installing..."; sudo apt-get install dd >/dev/null 2>&1; checkpacket $? dd; }
    # --------------------------------------------------------------------------
    # Benchmark loop
    # --------------------------------------------------------------------------
    echo '[LAUNCH] Benchmarking based on dd command...';
  
    for (( i = 1; i <= $repeats ; i++ )); do
        #Indicator
        p=$(( $i * 100 / $repeats))
        l=$(seq -s "+" $i | sed 's/[0-9]//g')

        # run dd command and get last line
        ${command_to_run} 2>&1| tail -1 >> $output_ddfile;

        #Clear RAM Memory Cache, Buffer and Swap Space on Linux
        sync && echo 3 > /proc/sys/vm/drop_caches
        #Indicator
        echo -ne ${l}' ('${p}'%) \r'
    done;
}
# ------------------------------
# 1.Create folder in current directory where this script is used to
# 2. Create inside new folder big-files and small-files
# ------------------------------
function create_filled(){
    folder_name=$1
    create="$(pwd)/utils/create-files.sh"
    #Delete if folder exist and recreate and refill
    if [ -d "$(pwd)/$folder_name/"  ]; then rm -r $(pwd)/$folder_name; fi
    mkdir -p mkdir $(pwd)/$folder_name $(pwd)/$folder_name/big-files $(pwd)/$folder_name/small-files;

    echo -e "\t [CREATE] big-files and small-files in [$(pwd)/$folder_name] ..."
    OUTPUT=$( source $create "$(pwd)/$folder_name/big-files" "$(pwd)/$folder_name/small-files")
    checktest $? $OUTPUT
}
#--------------------------------------------------------------------------
# Test 1: 
#       -Read n times to s files (n repeats, s file size)
#       -Read n times to s files (n repeats, s file size)
# By default n repeats=10
#--------------------------------------------------------------------------
#layer_folder=([level layer]='Folder name')
layer_folder=([10]='/test' [5]='/test2' [1]='/test3' [0]="$(pwd)/test-current-layer")
volumen_folder='/usr/src/app/myvolumen'
function simple_test(){
    #Scripts
    write="$(pwd)/tests/write-files-by-size.sh"
    read="$(pwd)/tests/read-file-by-size.sh"
    layer=$1
    #Filter name
    name=${2//[^[:alnum:]]/}
    for script in $write $read;do 
        if [[ $script == *"read"* ]]; then 
            current_script="read"
        else 
            current_script="write"
        fi
        echo "[LAUNCH]\[SIMPLE TEST] Script of $current_script in folder: $layer"
        for size in 1 8 32 128; do # UNIT M
            #Delete old test result
            rm $(pwd)/results/$current_script-$size-big-file-$name 2>>/dev/null
            for (( i = 1; i <= $repeats ; i++ ));do
                OUTPUT=$( source $script $layer"/big-files/" $size)
                checktest $? $OUTPUT
                echo $OUTPUT >> $(pwd)/results/$current_script-$size-big-file-$name
            done
        done    
    done
    #Change permissions
    chmod 666 -R $(pwd)/results/*
}

function init_simple_test(){
     if [ ! -z "$1" ];then layer=$1;else checktest -1 'Not folder specified';fi

    #--------------------------------------------------------------------------
    #  Test in specific folder
    #--------------------------------------------------------------------------
    if [[ ! $layer == *"all"* ]]; then
        simple_test "$layer" "only_$layer"
        exit 0
    fi

    #--------------------------------------------------------------------------
    #  Test in all available folder
    #--------------------------------------------------------------------------
    if [[ $layer == *"all"* ]]; then #Check all layers available   
        #--------------------------------------------------------------------------
        #  Create news file on R/W layer
        #--------------------------------------------------------------------------
        create_filled 'test-current-layer'    
        for i in "${!layer_folder[@]}"; do
            simple_test "${layer_folder[i]}" "$i"
        done
        simple_test "$volumen_folder" "volumen"
    fi
}
#--------------------------------------------------------------------------
# Test 2: 
#       -Read n times to s files (n repeats, s file size)
#       -Read n times to s files (n repeats, s file size)
# By default folder /test generates from Dockerfile.base
# Example launch: ./benchmark-command.sh -n 10 -t 2 -l 1
#--------------------------------------------------------------------------
function init_test(){
    #--------------------------------------------------------------------------
    #  Create news file on R/W layer
    #--------------------------------------------------------------------------
    create_filled 'test-current-layer'

    #--------------------------------------------------------------------------
    #   Test layer created in Base Image, Bind mount and R/W layer
    #--------------------------------------------------------------------------
    tests $volumen_folder
}
function tests(){
    #Tests of test folder
    #1. Inside of container
    write="$(pwd)/tests/write-to-files.sh"
    read="$(pwd)/tests/read-to-files.sh"
    current_script=''
    current_layer=false
    for layer in "${!layer_folder[@]}";do #layer = index position array
        echo -e "\t [LAUNCH] Test 2 in ${layer_folder[layer]} ..."
        folder=${layer_folder[layer]}
        for script in $write $read;do 
        if [[ $script == *"read"* ]]; then current_script="read";else current_script="write";fi  
            for (( i = 1; i <= $repeats ; i++ ));do
                OUTPUT=$( source $script "$folder/big-files/")
                checktest $? $OUTPUT
                echo $OUTPUT >> $(pwd)/results/$current_script-big-file-$layer
                OUTPUT=$( source $script "$folder/small-files/")
                checktest $? $OUTPUT
                echo $OUTPUT >> $(pwd)/results/$current_script-small-file-$layer    
            done 
        done 

    done
    #Volumen
    if [ ! -z "$1" ];then
        for script in $write $read;do 
        if [[ $script == *"read"* ]]; then current_script="read";else current_script="write";fi  
            for (( i = 1; i <= $repeats ; i++ ));do
                OUTPUT=$( source $script "$1/big-files/")
                checktest $? $OUTPUT
                echo $OUTPUT >> $(pwd)/results/$current_script-big-file-volumen
                OUTPUT=$( source $script "$1/small-files/")
                checktest $? $OUTPUT
                echo $OUTPUT >> $(pwd)/results/$current_script-small-file-volumen   
            done 
        done
    fi 

}

#
# === MAIN ===
#

if [ $(id -u) -ne 0 ]; then
	echo "[WARN] Run this script as a Root user only" >&2
	exit 1
fi

# Option parsing
while getopts n:c:o:t:l:s: OPT
do
    case "$OPT" in
        n) repeats=$OPTARG;;
        o) output_file=$OPTARG;;
        c) command_to_run=$OPTARG;;
        t) type_test=$OPTARG;;
        s) folder=$OPTARG;;
        \?)
            echo 'No arguments supplied'
            exit 1;;
    esac
done

re='^[0-9]+$'
if ! [[ $type_test =~ $re ]] ; then
   check 1 "It's necessary to indicate type test (-t[1,2,3,4])"
fi

if [ $type_test -eq 1 ];then
    init_simple_test $folder;
elif [ $type_test -eq 2 ];then
    init_test
elif [ $type_test -eq 3 ];then
    run_ddtests
elif [ $type_test -eq 4 ];then
    run_tests
else
    check 1 "Not valid type test (-t[1,2,3,4])"
fi
